String.prototype.nl2br = function(){
    return this.replace(/\n/g, "<br />");
}

function setTitle(title){
	if (title !== undefined) {
		document.title = title;
	}
}

function pageActivate(number, total, path){
	$(document).ready(function(){
		$(".pagination a").click(function(e){ 
	    	e.preventDefault(); 
	        var posting = $.post($(this).attr('href') + '/async');
	        if(path === 'blogs'){
	        	posting.done(createBlogsPage);
	    	}
	    	else if(path === 'profiles'){
	    		posting.done(createProfilesPage);
	    	}
	        return false;
    	});
		$('.pagination a:contains("' + number + '")').parent().addClass("active");
		if(number == 1){
			$('.pagination a[aria-label="First"]').parent().addClass("disabled");
		}
		if(number == total){
			$('.pagination a[aria-label="Last"]').parent().addClass("disabled");
		}
	});
}

function validateAuth(){
	$(document).ready(function(){
		$("[name='log']").click(function(e){
			if(!$.trim($('[name="logLogin"]').val()) || !$.trim($('[name="logPassword"]').val())){
		    	e.preventDefault(); 
		        $('#loginErr').removeClass('hidden');
	        	return false;
	    	}
    	});
		$("[name='logLogin']").on('input', function(){
	        $('#loginErr').addClass('hidden');
    	});
    	$("[name='logPassword']").on('input', function(){
	        $('#loginErr').addClass('hidden');
    	});

    	$("[name='reg']").click(function(e){
    		var obj = $('[name="regLogin"]');
			if(!$.trim(obj.val())){
		    	e.preventDefault(); 
		        $('#regLoginErr').removeClass('hidden');
		        obj.parents('.form-group').addClass('has-error');
	    	}
	    	obj = $('[name="regPassword"]');
	    	if(!$.trim(obj.val())){
		    	e.preventDefault(); 
		        $('#regPasswordErr').removeClass('hidden');
		        obj.parents('.form-group').addClass('has-error');
	    	}
	    	obj = $('[name="name"]');
	    	if(!$.trim(obj.val())){
		    	e.preventDefault(); 
		        $('#regNameErr').removeClass('hidden');
		        obj.parents('.form-group').addClass('has-error');
	    	}
	    	obj = $('[name="email"]');
	    	if(!$.trim(obj.val()) || !isEmail(obj.val())){
		    	e.preventDefault(); 
		        $('#regEmailErr').removeClass('hidden');
		        obj.parents('.form-group').addClass('has-error');
		        return false;
	    	}
    	});
		$("[name='regLogin']").on('input', function(){
	        $('#regLoginErr').addClass('hidden');
	        $(this).parents('.form-group').removeClass('has-error');
    	});
    	$("[name='regPassword']").on('input', function(){
	        $('#regPasswordErr').addClass('hidden');
	        $(this).parents('.form-group').removeClass('has-error');
    	});
    	$("[name='name']").on('input', function(){
	        $('#regNameErr').addClass('hidden');
	        $(this).parents('.form-group').removeClass('has-error');
    	});
    	$("[name='email']").on('input', function(){
	        $('#regEmailErr').addClass('hidden');
	        $(this).parents('.form-group').removeClass('has-error');
    	});
	});
}

function isEmail(email){
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);
}

function validateBlog(e){
	$(document).ready(function(){
		var obj = $('[name="text"]');
		if(!$.trim(obj.val())){
	    	e.preventDefault();
	        $('#emptyBlogErr').removeClass('hidden');
	        obj.parents('.form-group').addClass('has-error');
		}
		obj = $('[name="caption"]');
		if(!$.trim(obj.val())){
	    	e.preventDefault();
	        $('#emptyCaptionErr').removeClass('hidden');
	        obj.parents('.form-group').addClass('has-error');
		}
	    $("[name='text']").on('input', function(){
			$('#emptyBlogErr').addClass('hidden');
			$(this).parents('.form-group').removeClass('has-error');
	    });
	    $("[name='caption']").on('input', function(){
			$('#emptyCaptionErr').addClass('hidden');
			$(this).parents('.form-group').removeClass('has-error');
	    });
    });
}

function validateComment(e){
	$(document).ready(function(){
		var obj = $('#text');
		if(!$.trim(obj.val())){
	    	e.preventDefault();
	        $('#emptyCommentErr').removeClass('hidden');
	        obj.parents('.form-group').addClass('has-error');
    	}
	    obj.on('input', function(){
			$('#emptyCommentErr').addClass('hidden');
			$(this).parents('.form-group').removeClass('has-error');
	    });
    });
}

function validateSearch(e){
	$(document).ready(function(){
		var obj = $('[name="pattern"]');
		if($.trim(obj.val()).length < 4){
	    	e.preventDefault();
	        $('#searchLengthErr').removeClass('hidden');
	        obj.parents('.form-group').addClass('has-error');
		}
	    obj.on('input', function(){
			$('#searchLengthErr').addClass('hidden');
			$(this).parents('.form-group').removeClass('has-error');
	    });
    });
}

function createBlogsPage(data){
	var obj = $.parseJSON(data);
	var pagination = $('.pagination');
	pagination.empty();
	$('<li>').append($('<a>', {href: '/blogs/page/1', 'aria-label': 'First'}).append($('<span>', {'aria-hidden': 'true', text: '\u00AB'}))).appendTo(pagination);
	$.each(obj.pagination, function(i, val){
		$('<li>').append($('<a>', {href: '/blogs/page/' + val, text: val})).appendTo(pagination);
	});
	$('<li>').append($('<a>', {href: '/blogs/page/' + obj.total_pages, 'aria-label': 'Last'}).append($('<span>', {'aria-hidden': 'true', text: '\u00BB'}))).appendTo(pagination);
	pageActivate(obj.cur_page, obj.total_pages, 'blogs');
	history.pushState(null, null, '/blogs/page/' + obj.cur_page);

	var container = $('.blogs');
	container.empty();
	$.each(obj.blogs, function(i, val){
		var num = val['Number'];
		$('<h2>').append($('<a>', {href: '/blogs/blog/' + num, html: val['Caption'].nl2br()})).appendTo(container);
		var user = $('<p>', {class: 'lead'});
		user.append($('<i>', {class: 'fa fa-user', 'aria-hidden':'true'}));
		user.append(' ');
		user.append($('<a>', {href: '/profiles/profile/' + val['user_id'], text: val['Name']}));
		user.appendTo(container);
		var date = $('<p>');
		date.append($('<i>', {class: 'fa fa-clock-o', 'aria-hidden':'true'}));
		date.append(' ' + DateTimeFromSQL(val['Date']['date']));
		date.appendTo(container);
		$('<p>', {html: val['Content'].nl2br()}).appendTo(container);
		$('<a>', {class: 'btn btn-primary', href: '/blogs/blog/' + num, text: 'Читать дальше '}).append($('<span>', {class: 'glyphicon glyphicon-chevron-right'})).appendTo(container);
		$('<hr>').appendTo(container);
	});
}

function createProfilesPage(data){
	var obj = $.parseJSON(data);
	var pagination = $('.pagination');
	pagination.empty();
	$('<li>').append($('<a>', {href: '/profiles/page/1', 'aria-label': 'First'}).append($('<span>', {'aria-hidden': 'true', text: '\u00AB'}))).appendTo(pagination);
	$.each(obj.pagination, function(i, val){
		$('<li>').append($('<a>', {href: '/profiles/page/' + val, text: val})).appendTo(pagination);
	});
	$('<li>').append($('<a>', {href: '/profiles/page/' + obj.total_pages, 'aria-label': 'Last'}).append($('<span>', {'aria-hidden': 'true', text: '\u00BB'}))).appendTo(pagination);
	pageActivate(obj.cur_page, obj.total_pages, 'profiles');
	history.pushState(null, null, '/profiles/page/' + obj.cur_page);

	var container = $('.profiles');
	container.empty();
	$.each(obj.users, function(i, val){
		var div = $('<div>', {class: 'media'});
		$('<div>', {class: 'media-left media-middle'}).append($('<img>', {class: 'media-object avatar-small', alt: 'Avatar', src: val.avatar})).appendTo(div);
		$('<div>', {class: 'media-body'}).append($('<h3>').append($('<a>', {href: '/profiles/profile/' + val.id, text: val.login}))).appendTo(div);
		div.appendTo(container);
		$('<hr>').appendTo(container);
	});
}

function showPath(){
	var path = $("[name='avatar_fake']").val();
	$("[name='avatar']").val(path.substr(path.lastIndexOf('\\') + 1));
}

function setDateListeners(){
	$(document).ready(function(){
		var finalDate = new Date();
		var datepickerFrom = $('.dateFrom');
		var datepickerTo = $('.dateTo');
		$('#day').on('change', function () {
    		if($(this).is(':checked')){
    			var startDate = new Date(finalDate.getFullYear(), finalDate.getMonth(), finalDate.getDate() - 1);
    			datepickerFrom.datepicker('update', startDate);
    			datepickerTo.datepicker('update', finalDate);
    		}
  		});
  		$('#threeDays').on('change', function () {
    		if($(this).is(':checked')){
    			var startDate = new Date(finalDate.getFullYear(), finalDate.getMonth(), finalDate.getDate() - 3);
    			datepickerFrom.datepicker('update', startDate);
    			datepickerTo.datepicker('update', finalDate);
    		}
  		});
  		$('#week').on('change', function () {
    		if($(this).is(':checked')){
    			var startDate = new Date(finalDate.getFullYear(), finalDate.getMonth(), finalDate.getDate() - 7);
    			datepickerFrom.datepicker('update', startDate);
    			datepickerTo.datepicker('update', finalDate);
    		}
  		});
  		$('#month').on('change', function () {
    		if($(this).is(':checked')){
    			var startDate = new Date(finalDate.getFullYear(), finalDate.getMonth() - 1, finalDate.getDate());
    			datepickerFrom.datepicker('update', startDate);
    			datepickerTo.datepicker('update', finalDate);
    		}
  		});
  		$('.date').datepicker().on('changeDate', function(e) {
  			var radios = $("input:radio");
    		radios.attr("checked", false);
    		radios.parent().removeClass("active");
    	});
	});
}

function DateTimeFromSQL(date){
	var months = new Array("января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря");
	var formatedDate = new Date(date.substr(0, date.lastIndexOf(':')));
	return formatedDate.getDate() + ' ' + months[formatedDate.getMonth()] + ' ' + formatedDate.getFullYear() + ' ' + 
			formatedDate.getHours() + ':' + formatedDate.getMinutes();
}