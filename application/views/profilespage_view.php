<script type="text/javascript">pageTitle = 'Пользователи | Tiny Blogz';</script>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
	        <div class="profiles">
	        	<?php for($i = 0; $i < count($data['users']); $i++):?>
		            <div class="media">
		                <div class="media-left media-middle">
		                    <img class="media-object avatar-small" src="<?php echo $data['users'][$i]['avatar'];?>" alt="Avatar">
		                </div>
		                <div class="media-body">
		                    <h3>
		                    	<a href="/profiles/profile/<?php echo $data['users'][$i]['id'];?>"><?php echo $data['users'][$i]['login'];?></a>
		                    </h3>
		                </div>
		            </div>
		            <hr>
	            <?php endfor;?>
	        </div>
        </div>
    </div>
</div>
<nav class="text-center">
    <ul class="pagination pagination-lg">
        <li>
            <a href="/profiles/page/1" aria-label="First">
                <span aria-hidden="true">&laquo;</span>
            </a>
        </li>
        <?php foreach ($data['pagination'] as $value):?>
        	<li><a href="/profiles/page/<?php echo $value?>"><?php echo $value?></a></li>
        <?php endforeach;?>
        <li>
            <a href="/profiles/page/<?php echo $data['total_pages']?>" aria-label="Last">
                <span aria-hidden="true">&raquo;</span>
            </a>
        </li>
    </ul>
</nav>
<script type="text/javascript">pageActivate(<?php echo ($data['cur_page'] . ', ' . $data['total_pages']);?>, 'profiles');</script>