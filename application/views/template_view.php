<!DOCTYPE html>
<html>
	<head>
	    <meta charset="utf-8">
	    <meta name="keywords" content="Tiny, blogs, mini, posts">
	    <meta name="description" content="Free Service for Sharing Your Thoughts via Posts and Comments">
	    <meta name="author" content="Titov Nikita">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css" />
	    <link rel="stylesheet" type="text/css" href="/css/bootstrap-datepicker3.min.css" />
	    <link rel="stylesheet" type="text/css" href="/css/styles.css" />
	    <script type="text/javascript" src="/js/jquery-2.2.1.min.js"></script>
	    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
	    <script type="text/javascript" src="/js/bootstrap-datepicker.min.js"></script>	    
	    <script type="text/javascript" src="/js/scripts.js"></script>
	    <script type="text/javascript">var pageTitle;</script>
	    <title>Tiny Blogz</title>
	</head>
	<body>
	
    	<!-- Navigation -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/main">Tiny Blogz</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="/profiles">Авторы</a>
                        </li>
                        <li class="dropdown">
                  			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  				Блоги <span class="caret"></span>
                  			</a>
                  			<ul class="dropdown-menu">
                    			<li><a href="/blogs">Читать</a></li>
                    			<li <?php if (!isset($_SESSION['login'])) { echo "class=hidden"; }?>><a href="/blogs/add">Добавить блог</a></li>
                    			<li><a href="/blogs/search">Поиск по блогам</a></li>
                  			</ul>
                		</li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
    	                <li><p class="navbar-text left-fix">Здравствуйте, <?php
    	    						$started = isset($_SESSION['full_name']);
    	        					echo $started ? $_SESSION['full_name'] : "Гость";
    	        		?></p></li>
    	                <li><p class="navbar-btn left-fix">
    	                    <a href="/auth" class="btn btn-default"><?php echo $started ? 'Выйти' : 'Войти';?></a>
    	                </p></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>
	    <?php 
	        include 'application/views/'. $content_view;
	    ?>
		<footer class="footer">
    		<div class="container">
				<p class="text-muted text-center">Copyright &copy; Tiny Blogz co. | 2016</p>
    		</div>
    	</footer>
	<script type="text/javascript">setTitle(pageTitle);</script>
	</body>
</html>