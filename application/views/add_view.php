<script type="text/javascript">pageTitle = 'Новый Блог | Tiny Blogz';</script>
<?php $show = !isset($_SESSION['login']) && !isset($data['error']['logged']); ?>
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="loginModalLabel">Требуется вход!</h4>
      		</div>
      		<div class="modal-body">
        		<p class="text-danger">Только зарегистрированные пользователи могут публиковать блоги.</p>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      		</div>
    	</div>
  	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<form role="form" class="form-horizontal well" autocomplete="off" action="/blogs/add" method="POST">
				<legend>Добавление блога</legend>
				<div class="form-group">
					<label for="caption" class="col-sm-2 control-label">Название*</label>
					<div class="col-sm-10">
						<textarea class="form-control" id="caption" name="caption" rows="1" maxlength="200" placeholder="Введите название"><?php
            				echo $data['info']['caption'];
        				?></textarea>
						<p id="emptyCaptionErr" class="help-block text-danger <?php if (!isset($data['error']['caption'])) { echo 'hidden'; }?>">
							Пожалуйста, введите название!
						</p>
					</div>
				</div>
				<div class="form-group">
					<label for="text" class="col-sm-2 control-label">Контент*</label>
					<div class="col-sm-10">
						<textarea class="form-control" id="text" name="text" rows="10" maxlength="15000000"><?php
            				echo $data['info']['text'];
        				?></textarea>
						<p id="emptyBlogErr" class="help-block text-danger <?php if (!isset($data['error']['textEmpty'])) { echo 'hidden'; }?>">
							Пожалуйста, введите тело блога!
						</p>
						<p class="help-block text-danger <?php if (!isset($data['error']['textLong'])) { echo 'hidden'; }?>">
							Блог должен быть менее 15 000 000 символов!
						</p>
						<p class="help-block text-danger <?php if (!isset($data['error']['logged'])) { echo 'hidden'; }?>">
							Для публикации блога вам необходимо зайти на сайт!
						</p>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-4">
						<button type="submit" class="btn btn-primary btn-lg btn-block" <?php if ($show) { echo 'disabled'; }?>
								onclick="validateBlog(event);">Разместить блог</button>
					</div>
				</div> 
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	var show = <?php if ($show) { echo 'true'; } else { echo 'false'; }?>;
	if(show){
		$('#loginModal').modal('show');
	}
</script>