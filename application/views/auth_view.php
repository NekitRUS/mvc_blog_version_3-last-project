<script type="text/javascript">pageTitle = 'Авторизация | Tiny Blogz';</script>
<div class="container">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<form  autocomplete="off" action="/auth" method="POST" role="form" class="form-horizontal well">
				<legend>Вход</legend>
				<div class="form-group">
					<label for="logLogin" class="col-sm-2 control-label">Логин</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="logLogin" name="logLogin" placeholder="Логин" value="">
					</div>
				</div>
				<div class="form-group">
					<label for="logPassword" class="col-sm-2 control-label">Пароль</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" id="logPassword" name="logPassword" placeholder="Пароль">
						<p class="help-block text-danger <?php if (!isset($data['error']['mismatch'])) { echo 'hidden'; }?>">
							Пожалуйста, проверьте корректность введенных данных
						</p>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-4">
						<button type="submit" name="log" class="btn btn-primary btn-lg btn-block">Залогиниться</button>
					</div>
				</div> 
			</form>
			<form  autocomplete="off" enctype="multipart/form-data" action="/auth" method="POST" role="form" class="form-horizontal well">
				<legend>Регистрация</legend>
				<div class="form-group">
					<label for="regLogin" class="col-sm-2 control-label">Логин*</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="regLogin" name="regLogin"
								placeholder="Логин" value="<?php echo $data['info']['regLogin']; ?>"/>
						<p id="regLoginErr" class="help-block text-danger <?php if (!isset($data['error']['login'])) { echo 'hidden'; }?>">
							Пожалуйста, введите логин
						</p>
					</div>
				</div>
				<div class="form-group">
					<label for="regPassword" class="col-sm-2 control-label">Пароль*</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" id="regPassword" name="regPassword"
								placeholder="Пароль" value="<?php echo $data['info']['regPassword']; ?>"/>
						<p id="regPasswordErr" class="help-block text-danger <?php if (!isset($data['error']['password'])) { echo 'hidden'; }?>">
							Пожалуйста, введите пароль
						</p>
					</div>
				</div>
				<div class="form-group">
					<label for="name" class="col-sm-2 control-label">ФИО*</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="name" name="name"
								placeholder="ФИО" value="<?php echo $data['info']['name']; ?>"/>
						<p id="regNameErr" class="help-block text-danger <?php if (!isset($data['error']['name'])) { echo 'hidden'; }?>">
							Пожалуйста, введите ФИО
						</p>
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-sm-2 control-label">E-mail*</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="email" name="email"
								placeholder="E-mail" value="<?php echo $data['info']['email']; ?>"/>
						<p id="regEmailErr" class="help-block text-danger <?php if (!isset($data['error']['email'])) { echo 'hidden'; }?>">
							Пожалуйста, введите корректный E-mail
						</p>
					</div>
				</div>
				<div class="form-group">
					<label for="vk" class="col-sm-2 control-label">VK</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="vk" name="vk"
								placeholder="Профиль в VK" value="<?php echo $data['info']['vk']; ?>"/>
					</div>
				</div>
				<div class="form-group">
					<label for="linkedIn" class="col-sm-2 control-label">LinkedIn</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="linkedIn" name="linkedIn"
								placeholder="Профиль в LinkedIn" value="<?php echo $data['info']['linkedIn']; ?>"/>
					</div>
				</div>
				<div class="form-group">
					<label for="twitter" class="col-sm-2 control-label">Twitter</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="twitter" name="twitter"
								placeholder="Профиль в Twitter" value="<?php echo $data['info']['twitter']; ?>"/>
					</div>
				</div>
				<div class="form-group">
					<label for="gplus" class="col-sm-2 control-label">Google+</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="gplus" name="gplus"
								placeholder="Профиль в Google+" value="<?php echo $data['info']['gplus']; ?>"/>
					</div>
				</div>
				<div class="form-group">
					<label for="avatar" class="col-sm-2 control-label">Аватар</label>
					<div class="col-sm-10">
						<input type="file" accept="image/jpeg,image/png,image/jpg" class="form-control hidden" name="avatar_fake" onchange="showPath();"/>
						<input type="text" class="form-control" id="avatar" name="avatar"
								placeholder="Выберите аватар" readonly="readonly" onclick="document.getElementsByName('avatar_fake')[0].click();"/>
						<p id="fileErr" class="help-block text-danger <?php if (!isset($data['error']['file'])) { echo 'hidden'; }?>">
							Пожалуйста, выберите .jpg, .jpeg или .png файл размером не более 700Kb!
						</p>
					</div>
				</div>
				<div class="form-group">
					<label for="details" class="col-sm-2 control-label">О себе</label>
					<div class="col-sm-10">
						<textarea class="form-control" id="details" name="details" rows="6" maxlength="65000" placeholder="Расскажите немного о себе"><?php
            				echo $data["info"]["details"];
        				?></textarea>
						<p class="help-block text-danger <?php if (!isset($data['error']['alreadyExists'])) { echo 'hidden'; }?>">
							Пользователь с таким логином или такой почтой уже существует!
						</p>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-4">
						<button type="submit" name="reg" class="btn btn-primary btn-lg btn-block">Зарегистрироваться</button>
					</div>
				</div> 
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">validateAuth();</script>