<?php
    $number = $data['info']['blog']['id'];
    $commentsCount = count($data['info']['comments']);
    $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $title = "Блог на сайте Tiny Blogz";
    //$title = iconv("Windows-1251","UTF-8", $title);
    $title = urlencode($title);
    $description = $data['info']['blog']['caption'];
    //$description = iconv("Windows-1251","UTF-8", $description);
    $description = urlencode($description);
?>
<script type="text/javascript">pageTitle = "Блог №<?php echo $number;?> | Tiny Blogz";</script>
<div class="container">
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1">
			<H1><?php echo nl2br($data['info']['blog']['caption'])?></H1>
			<p class="lead">
				<i class="fa fa-user" aria-hidden="true"></i> <a href=/profiles/profile/<?php echo $data['info']['blog']['user_id'];?>><?php
					echo $data['info']['blog']['user_login'];?></a>
			</p>
			<p><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $this->DateTimeFromSQL($data['info']['blog']['date'], 'j F Y G:i');?></p>
			<p class="lead"><?php echo nl2br($data['info']['blog']['content']);?></p>
			<p class="text-right">
				Поделиться: <a
	            	title="Опубликовать ссылку в LinkedIn"
	            	target="_blank"
	            	href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url;?>"><i aria-hidden="true" id="social-li" class="fa fa-linkedin fa-2x social"></i></a>
	            <a
	            	title="Опубликовать ссылку в Twitter"
	            	target="_blank"
	            	href="http://twitter.com/share?url=<?php echo $url;?>&text=<?php echo (strlen($description) > 80 ? substr($description . "...", 0, 80) : $description);?>&via=@tinyBlogz"><i aria-hidden="true" id="social-tw" class="fa fa-twitter fa-2x social"></i></a>
	            <a
	            	title="Опубликовать ссылку в Google Plus"
	            	target="_blank"
	            	href="https://plus.google.com/share?url=<?php echo $url;?>"><i aria-hidden="true" id="social-gp" class="fa fa-google-plus fa-2x social"></i></a>
	            <a
	            	title="Опубликовать ссылку во Вконтакте"
            		target="_blank"
	            	href="http://vk.com/share.php?url=<?php echo $url;?>&title=<?php echo $title;?>&description=<?php echo $description;?>"><i aria-hidden="true" id="social-vk" class="fa fa-vk fa-2x social"></i></a>
			</p>
			<hr>
			<form  autocomplete="off" action=<?php echo "'/blogs/blog/" . $data['info']['blog']['id'] . "'";?>
                    method="POST" role="form" class="form-horizontal well">
                <legend>Оставить комментарий</legend>
                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea id="text" class="form-control" name="text" rows="6" maxlength="65000"><?php echo $data["input"]["text"];?></textarea>
                        <p class="help-block text-danger <?php if (isset($_SESSION['login']) || isset($data['error']['logged'])) { echo 'hidden'; }?>">
                                Только зарегистрированные пользователи могут оставлять комментарии
                        </p>
                        <p id="emptyCommentErr" class="help-block text-danger <?php if (!isset($data['error']['textEmpty'])) { echo 'hidden'; }?>">
                                Пожалуйста, введите комментарий!
                        </p>
                        <p class="help-block text-danger <?php if (!isset($data['error']['textLong'])) { echo 'hidden'; }?>">
                                Комментарий должен быть менее 65 000 символов!
                        </p>
                        <p class="help-block text-danger <?php if (!isset($data['error']['logged'])) { echo 'hidden'; }?>">
                                Для публикации комментария вам необходимо зайти на сайт!
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4">
                        <button type="submit" class="btn btn-primary btn-lg btn-block" <?php if (!isset($_SESSION['login'])) { echo 'disabled'; }?>
                                onclick="validateComment(event);">Отправить</button>
                    </div>
                </div> 
            </form>
            <hr>
            <div class="page-header">
                <H1>Комментарии (<?php echo $commentsCount;?>)</H1>
            </div>
            <?php for ($i = $commentsCount - 1; $i >= 0 ; $i--):?>
            <div class="media">
                <div class="media-left">
                    <a href="/profiles/profile/<?php echo $data['info']['comments'][$i]['user_id'];?>">
                        <img class="media-object avatar-small" src="<?php echo $data['info']['comments'][$i]['avatar'];?>" alt="Avatar">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading"><a href="/profiles/profile/<?php echo $data['info']['comments'][$i]['user_id'];?>"><?php
                        echo $data['info']['comments'][$i]['user_login'];?></a>
                        <small><?php
                            echo $this->DateTimeFromSQL($data['info']['comments'][$i]['date'], 'j F Y') . ' в ' . 
                            $this->DateTimeFromSQL($data['info']['comments'][$i]['date'], 'G:i:s');
                        ?></small>
                    </h4><?php
                        echo nl2br($data['info']['comments'][$i]['text']);?>
                </div>
            </div>
            <?php endfor;?>
		</div>
	</div>
</div>