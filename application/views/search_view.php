<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <form role="form" class="form-horizontal well" autocomplete="off" action="/blogs/search" method="POST">
                <legend>Поиск</legend>
                <div class="form-group">
                    <label for="pattern" class="col-sm-2 control-label">Что искать*</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="pattern" name="pattern" placeholder="Текст запроса" value="">
                        <p id="searchLengthErr" class="help-block text-danger hidden">
                            Поисковый запрос должен содержать не менее 4-х символов.
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="from" class="col-sm-2 control-label">Когда</label>
                    <div class="col-sm-5 ">
                        <div class="input-group date dateFrom">
                            <input type="text" class="form-control" id="from" name="from" placeholder="От" value="" readonly="readonly">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-5 ">
                        <div class="input-group date dateTo">
                            <input type="text" class="form-control" id="to" name="to" placeholder="До" value="" readonly="readonly">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                	<label for="day" class="col-sm-2 control-label">За</label>
                	<div class="col-sm-10">
		                <div class="btn-group btn-group-justified" data-toggle="buttons">
							<label class="btn btn-default">
								<input type="radio" name="period" id="day" autocomplete="off">сутки
							</label>
							<label class="btn btn-default">
								<input type="radio" name="period" id="threeDays" autocomplete="off">три дня
							</label>
							<label class="btn btn-default">
								<input type="radio" name="period" id="week" autocomplete="off">неделю
							</label>
							<label class="btn btn-default">
								<input type="radio" name="period" id="month" autocomplete="off">месяц
							</label>
						</div>
					</div>
				</div>
                <div class="form-group">
                    <div class="col-sm-6 col-sm-offset-4">
                        <button type="submit" class="btn btn-primary btn-lg btn-block" onclick="validateSearch(event);">Найти</button>
                    </div>
                </div> 
            </form>
        </div>
    </div>
</div>


<div class="container <?php if(!isset($data['blogs'])){echo 'hidden';}?>">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="page-header">
                <H1>Результаты (<?php echo count($data['blogs']);?>)</H1>
            </div>
            <?php 
                include 'application/views/blog_part.php';
            ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    pageTitle = 'Поиск | Tiny Blogz';
    $('.input-group.date').datepicker({
        format: 'dd.mm.yyyy',
        language: 'ru',
        autoclose: true,
        disableTouchKeyboard: true,
        zIndexOffset: 10000,
        clearBtn: true
    });
    setDateListeners();
</script>