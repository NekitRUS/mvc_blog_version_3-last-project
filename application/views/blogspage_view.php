<script type="text/javascript">pageTitle = 'Блоги | Tiny Blogz';</script>
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <?php 
                include 'application/views/blog_part.php';
            ?>
            <nav class="text-center">
                <ul class="pagination pagination-lg">
                    <li>
                        <a href="/blogs/page/1" aria-label="First">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <?php foreach ($data['pagination'] as $value):?>
                        <li><a href="/blogs/page/<?php echo $value?>"><?php echo $value?></a></li>
                    <?php endforeach;?>
                    <li>
                        <a href="/blogs/page/<?php echo $data['total_pages']?>" aria-label="Last">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
<script type="text/javascript">pageActivate(<?php echo ($data['cur_page'] . ', ' . $data['total_pages']);?>, 'blogs');</script>