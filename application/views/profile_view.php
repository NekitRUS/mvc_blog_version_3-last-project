<script type="text/javascript">pageTitle = "Пользователь <?php echo $data['user']['login'];?> | Tiny Blogz";</script>
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="media">
                <div class="media-left">
                    <img class="media-object avatar-big" src="<?php echo $data['user']['avatar'];?>" alt="Avatar">
                </div>
                <div class="media-body">
                    <h2 class="media-heading"><?php echo $data['user']['login'];?></h2>
                    <ul class="list-group no-border">
                        <li class="list-group-item"><i class="fa fa-user fa-2x fa-fw" aria-hidden="true"></i><?php echo $data['user']['full_name'];?></li>
                        <li class="list-group-item"><i class="fa fa-envelope fa-2x fa-fw" aria-hidden="true"></i>
                            <a href="mailto:<?php echo $data['user']['e_mail'];?>">Отправить E-mail</a>
                        </li>
                        <?php
                            if (!empty($data['user']['vk'])) {
                                echo "<li class='list-group-item'><i class='fa fa-vk fa-2x fa-fw' aria-hidden='true'></i>
                                        <a target='_blank' href=" . $data['user']['vk'] . ">Профиль в VK</a></li>";
                            }
                            if (!empty($data['user']['linkedIn'])) {
                                echo "<li class='list-group-item'><i class='fa fa-linkedin fa-2x fa-fw' aria-hidden='true'></i>
                                        <a target='_blank' href=" . $data['user']['linkedIn'] . ">Профиль в LinkedIn</a></li>";
                            }
                            if (!empty($data['user']['twitter'])) {
                                echo "<li class='list-group-item'><i class='fa fa-twitter fa-2x fa-fw' aria-hidden='true'></i>
                                        <a target='_blank' href=" . $data['user']['twitter'] . ">Профиль в Twitter</a></li>";
                            }
                            if (!empty($data['user']['gplus'])) {
                                echo "<li class='list-group-item'><i class='fa fa-google-plus fa-2x fa-fw' aria-hidden='true'></i>
                                        <a target='_blank' href=" . $data['user']['gplus'] . ">Профиль в Google+</a></li>";
                            }
                            if (!empty($data['user']['details'])) {
                                echo "<li class='list-group-item'><i class='fa fa-comment fa-2x fa-fw' aria-hidden='true'></i>
                                        О себе: <br/>" . $data['user']['details'] . "</li>";
                            }?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>