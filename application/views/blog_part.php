<div class="blogs">
    <?php for($i = 0; $i < count($data['blogs']); $i++):
        $num = $data['blogs'][$i]['Number'];?>
        <h2>
            <a href="/blogs/blog/<?php echo $num;?>"><?php echo nl2br($data['blogs'][$i]['Caption']);?></a>
        </h2>
        <p class="lead">
            <i class="fa fa-user" aria-hidden="true"></i> <a href="/profiles/profile/<?php echo $data['blogs'][$i]['user_id'];?>"><?php
                echo $data['blogs'][$i]['Name'];?></a>
        </p>
        <p><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $this->DateTimeFromSQL($data['blogs'][$i]['Date'], 'j F Y G:i');?></p>
        <p><?php echo nl2br($data['blogs'][$i]['Content']);?></p>
        <a class="btn btn-primary" href="/blogs/blog/<?php echo $num;?>">Читать дальше <span class="glyphicon glyphicon-chevron-right"></span></a>
        <hr>
    <?php endfor;?>
</div>