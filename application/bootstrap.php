<?php
if (version_compare(phpversion(), '7.0.0', '<=')) {
    die('PHP version 7.0.0 and higher is required!');
}

$config = parse_ini_file('config.ini');
ini_set('display_errors', $config['ERRORS']);
ini_set('date.timezone', $config['TIMEZONE']);
require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/controller.php';
require_once 'core/route.php';

Route::Start();