<?php
Class Controller_profiles extends Controller{

    function __construct(){

        $this->model = new Model_profiles();
        $this->view = new View();
        //session_id(1); //Local virtual host fix...
        session_start();
    }
    
    function action_index(){

        $this->action_page('1');
    }

    function action_page($number, $async = false){

        if(!$this->IsPositiveInteger($number)){
            Route::ErrorPage404();
            exit;
        }

        $data = $this->model->GetUsers((int)$number);

        $data['cur_page'] = (int)$number;
        if($data['cur_page'] > $data['total_pages']){
            Route::ErrorPage404();
            exit;
        }

        $data['pagination'] = $this->Pagination($data['cur_page'], $data['total_pages']);

        if($async === 'async'){
            echo json_encode($data);
            exit;
        }
        $this->view->Generate('profilespage_view.php', 'template_view.php', $data);
    }
    
    function action_profile($userID){

    	if(!$this->IsPositiveInteger($userID)){
            Route::ErrorPage404();
            exit;
        }

        $result = $this->model->GetUser($userID);
        if ($result) {
            $data['user'] = $result;
        }
        else {
            Route::ErrorPage404();
            exit;
        }
        
        $this->view->Generate('profile_view.php', 'template_view.php', $data);
    }
}