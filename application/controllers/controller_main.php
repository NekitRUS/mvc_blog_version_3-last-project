<?php
Class Controller_main extends Controller{

    function __construct(){

        $this->model = new Model();
        $this->view = new View();
        //session_id(1); //Local virtual host fix...
        session_start();
    }
    
    function action_index(){

        $this->view->Generate('main_view.php', 'template_view.php', $data);
    }
}