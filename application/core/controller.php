<?php
class Controller{
	
    public $model;
    public $view;
    
    function __construct(){

        $this->view = new View();
    }

    function IsPositiveInteger($value){

    	return (ctype_digit($value) && ((int)$value > 0)) ? true : false;
    }

    function Pagination($curPage, $maxPage){

        $array = [];
        for($i = $curPage - 1; ($i > 0) && ($curPage - $i < 3); $i--){
            $array[] = $i;
        }
        for($i = $curPage; ($i <= $maxPage) && ($i - $curPage < 3); $i++){
            $array[] = $i;
        }
        sort($array);
        return $array;
    }
    
    function action_index(){
    }
}