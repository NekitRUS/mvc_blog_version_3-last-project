<?php
class Model_profiles extends Model{
    
    function GetUser($userID){

        $array = [];
        if ($stmt = $this->GetConnection()->prepare("SELECT * FROM users WHERE id = ?")){
            if ($stmt->bind_param('i', $idINT = (int)$userID)){
                if ($stmt->execute()){
                    if($stmt->bind_result($id, $login, $password, $salt, $e_mail, $full_name, $vk, $linkedIn, $twitter, $gplus, $avatar, $details)){
                        if($stmt->fetch()){
                            $stmt->close();
                            return $array['profile'] =  ['login' => htmlspecialchars($login, ENT_HTML5),
                                            'full_name' => htmlspecialchars($full_name, ENT_HTML5),
                                            'e_mail' => htmlspecialchars($e_mail, ENT_HTML5),
                                            'vk' => htmlspecialchars($vk, ENT_HTML5),
                                            'linkedIn' => htmlspecialchars($linkedIn, ENT_HTML5),
                                            'twitter' => htmlspecialchars($twitter, ENT_HTML5),
                                            'gplus' => htmlspecialchars($gplus, ENT_HTML5),
                                            'avatar' => htmlspecialchars($avatar, ENT_HTML5),
                                            'details' => htmlspecialchars($details, ENT_HTML5)];
                        } 
                    } 
                }
            }
        }
        $stmt->close();
        return false; 
    }
    
    function GetUsers($start){

        $array = ['users' => []];
        $total = 1;

        $result = $this->GetConnection()->query("SELECT COUNT(*) FROM users");
        if($result){
            $row = $result->fetch_row();
            $total = (int)$row[0];
            if(($total % 10) != 0){
                $total = (int)($total / 10) + 1;
            }
            else{
                $total = $total / 10;
            }
            $array['total_pages'] = $total;
        }

        if($start > $total){
            return $array;
        }

        $start = $start * 10 - 10;
        $result = $this->GetConnection()->query("SELECT id, login, avatar
                FROM users
                ORDER BY login
                LIMIT $start, 10");
        if($result){
            while($row = $result->fetch_array()){
                $array['users'][] = ['id' => (int)$row['id'],
                            'login' => htmlspecialchars($row['login'], ENT_HTML5),
                            'avatar' => htmlspecialchars($row['avatar'], ENT_HTML5)];
            }
        }
        return $array;
    }
}