<?php
class Model_blogs extends Model{

    function GetBlogs($start){

        $array = ['blogs' => []];
        $total = 1;

        $result = $this->GetConnection()->query("SELECT COUNT(*) FROM blogs");
        if($result){
        	$row = $result->fetch_row();
            $total = (int)$row[0];
			if(($total % 5) != 0){
				$total = (int)($total / 5) + 1;
			}
			else{
				$total = $total / 5;
			}
			$array['total_pages'] = $total;
        }

        if($start > $total){
        	return $array;
        }

        $start = $start * 5 - 5;
        $result = $this->GetConnection()->query("SELECT blogs.id AS id, user_login, date, caption, LEFT(content, 1000), e_mail, users.id AS user_id
                 FROM blogs JOIN users
                 ON blogs.user_login = users.login
                 ORDER BY blogs.id DESC
                 LIMIT $start, 5");
        if($result){
            while($row = $result->fetch_array()){
                $array['blogs'][] = ['Name' => htmlspecialchars($row['user_login'], ENT_HTML5),
                            'Email' => htmlspecialchars($row['e_mail'], ENT_HTML5),
                            'Caption' => htmlspecialchars($row['caption'], ENT_HTML5),
                            'Content' => htmlspecialchars($row['LEFT(content, 1000)'], ENT_HTML5),
                            'Date' => new DateTime($row['date']),
                            'Number' => (int)$row['id'],
                            'user_id' => (int)$row['user_id']];
            }
        }

        return $array;
    }

    function InsertBlog($obj){

        if ($stmt = $this->GetConnection()->prepare("INSERT INTO blogs (user_login, caption, content, date) VALUES (?, ?, ?, ?)")){
            if ($stmt->bind_param('ssss', $obj['Name'], $obj['Caption'], $obj['Content'], $date = $this->DateTimeForSQL(new DateTime()))){
                $result = $stmt->execute();
                $stmt->close();
                return $result;
            }
        }
        $stmt->close();
        return false; 
    }
    
    function InsertComment($obj){

        if ($stmt = $this->GetConnection()->prepare("INSERT INTO comments (blog_id, user_login, text, date) VALUES (?, ?, ?, ?)")){
            if ($stmt->bind_param('isss', $idINT = (int)$obj['blog_id'], $obj['user_login'], $obj['text'], $date = $this->DateTimeForSQL(new DateTime()))){
                $result = $stmt->execute();
                $stmt->close();
                return $result;
            }
        }
        $stmt->close();
        return false; 
    }
    
    function GetBlog($blog){

        $array = [];
        $idINT = (int)$blog;
        
        if ($stmt = $this->GetConnection()->prepare("SELECT blogs.id AS id, user_login, date, caption, content, e_mail, users.id AS user_id
                                                    FROM blogs JOIN users
                                                    ON blogs.user_login = users.login
                                                    WHERE blogs.id = ?")){
            if ($stmt->bind_param('i', $idINT)){
                if ($stmt->execute()){
                    if($stmt->store_result()){
                        if($stmt->bind_result($id, $user_login, $date, $caption, $content, $e_mail, $user_id)){
                            if($stmt->fetch()){
                                $stmt->free_result();
                                $stmt->close();
                                $array['blog'] = ['id' => $id,
                                                'user_login' => htmlspecialchars($user_login, ENT_HTML5),
                                                'caption' => htmlspecialchars($caption, ENT_HTML5),
                                                'content' => htmlspecialchars($content, ENT_HTML5),
                                                'date' => new DateTime($date),
                                                'e_mail' => htmlspecialchars($e_mail, ENT_HTML5),
                                                'user_id' => $user_id];
                            }
                            else{
                                return false;
                            }
                        }
                    }
                }
            }
        }
        
        $array['comments'] = [];
        if ($stmt = $this->GetConnection()->prepare("SELECT comments.user_login, comments.date, comments.text, users.e_mail, users.id, users.avatar
                                                    FROM comments
                                                    JOIN users
                                                    ON comments.user_login = users.login
                                                    WHERE blog_id = ?
                                                    ORDER BY comments.date")){
            if ($stmt->bind_param('i', $idINT)){
                if ($stmt->execute()){
                    if($stmt->store_result()){
                        if($stmt->bind_result($user_login, $date, $text, $e_mail, $id, $avatar)){
                            while($stmt->fetch()){
                                $array['comments'][] = ['user_login' => htmlspecialchars($user_login, ENT_HTML5),
                                                'e_mail' => htmlspecialchars($e_mail, ENT_HTML5),
                                                'text' => htmlspecialchars($text, ENT_HTML5),
                                                'date' => new DateTime($date),
                                                'user_id' => htmlspecialchars($id, ENT_HTML5),
                                                'avatar' => htmlspecialchars($avatar, ENT_HTML5)];
                            }
                            $stmt->free_result();
                            $stmt->close();
                        }
                    }
                }
            }
        }
        
        return $array;
    }

    function Search($obj){

        $array = ['blogs' => []];
        try{
            if($obj['from']){
                $from = new DateTime($obj['from']);
            }
            else{
                throw new Exception('Null Date.');
            }
        }
        catch(Exception $e){
            $from = new DateTime('1970-01-01 00:00:00');
        }
        $from =  $this->DateTimeForSQL($from);
        try{
            $to = new DateTime($obj['to']);
        }
        catch(Exception $e){
            $to = new DateTime();
        }
        $to =  $this->DateTimeForSQL($to);

        if ($stmt = $this->GetConnection()->prepare("SELECT blogs.id AS id, user_login, date, caption, LEFT(content, 1000) AS body, e_mail, users.id AS user_id
                                                    FROM blogs JOIN users
                                                    ON blogs.user_login = users.login
                                                    WHERE MATCH(caption, content) AGAINST(?)
                                                    AND date BETWEEN ? AND ?
                                                    LIMIT 10")){
            if ($stmt->bind_param('sss', $obj['pattern'], $from, $to)){
                if ($stmt->execute()){
                    if($stmt->store_result()){
                        if($stmt->bind_result($id, $user_login, $date, $caption, $body, $e_mail, $user_id)){
                            while($stmt->fetch()){
                                $array['blogs'][] = ['Name' => htmlspecialchars($user_login, ENT_HTML5),
                                                    'Email' => htmlspecialchars($e_mail, ENT_HTML5),
                                                    'Caption' => htmlspecialchars($caption, ENT_HTML5),
                                                    'Content' => htmlspecialchars($body, ENT_HTML5),
                                                    'Date' => new DateTime($date),
                                                    'Number' => (int)$id,
                                                    'user_id' => (int)$user_id];
                            }
                            $stmt->free_result();
                            $stmt->close();
                        }
                    }
                }
            }
        }

        return $array;
    }
}